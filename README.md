# Game Tic Tac Toc - WASM.

## Description

the project is composed by two projects, the main is a rust wams. the second is a frontend build with vuejs 3.

## User

1. In the main project execute the command `wasm-pack build`
2. Change to folder web-app and execute the command `npm i`
3. Execute the command `npm run dev`
4. Open the browser and test the game.

## Link

https://game-tic-tac-toc-macordoba-d56fcdf5e05e31e511214d90bc62cf411c7a.gitlab.io/