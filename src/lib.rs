mod structure;

use crate::structure::Player;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn validate_winner(plays: i32, board: Box<[Player]>) -> Player {
    if plays >= 5 {
        if (Player::O == board[0] && board[0] == board[1] && board[1] == board[2])
            || (Player::O == board[3] && board[3] == board[4] && board[4] == board[5])
            || (Player::O == board[6] && board[6] == board[7] && board[7] == board[8])
        {
            return Player::O;
        } else if (Player::X == board[0] && board[0] == board[1] && board[1] == board[2])
            || (Player::X == board[3] && board[3] == board[4] && board[4] == board[5])
            || (Player::X == board[6] && board[6] == board[7] && board[7] == board[8])
        {
            return Player::X;
        }

        if (Player::O == board[0] && board[0] == board[3] && board[3] == board[6])
            || (Player::O == board[1] && board[1] == board[4] && board[4] == board[7])
            || (Player::O == board[2] && board[2] == board[5] && board[5] == board[8])
        {
            return Player::O;
        } else if (Player::X == board[0] && board[0] == board[3] && board[3] == board[6])
            || (Player::X == board[1] && board[1] == board[4] && board[4] == board[7])
            || (Player::X == board[2] && board[2] == board[5] && board[5] == board[8])
        {
            return Player::X;
        }

        if (Player::O == board[0] && board[0] == board[4] && board[4] == board[8])
            || (Player::O == board[2] && board[2] == board[4] && board[4] == board[6])
        {
            return Player::O;
        } else if (Player::X == board[0] && board[0] == board[4] && board[4] == board[8])
            || (Player::X == board[2] && board[2] == board[4] && board[4] == board[6])
        {
            return Player::X;
        }
    }

    Player::None
}

#[test]
fn test_draw() {
    let board = Box::new([
        Player::O,
        Player::O,
        Player::X,
        Player::X,
        Player::X,
        Player::O,
        Player::O,
        Player::X,
        Player::O,
    ]);

    assert_eq!(validate_winner(9, board), Player::None);
}

#[test]
fn test_win_vertical() {
    let board = Box::new([
        Player::O,
        Player::X,
        Player::O,
        Player::O,
        Player::X,
        Player::None,
        Player::None,
        Player::X,
        Player::None,
    ]);

    assert_eq!(validate_winner(6, board), Player::X);
}

#[test]
fn test_horizontal() {
    let board = Box::new([
        Player::None,
        Player::X,
        Player::X,
        Player::O,
        Player::O,
        Player::O,
        Player::None,
        Player::None,
        Player::None,
    ]);

    assert_eq!(validate_winner(5, board), Player::O);
}

#[test]
fn test_diagonal() {
    let board = Box::new([
        Player::O,
        Player::None,
        Player::X,
        Player::None,
        Player::O,
        Player::None,
        Player::None,
        Player::X,
        Player::O,
    ]);

    assert_eq!(validate_winner(5, board), Player::O);
}
