use wasm_bindgen::prelude::*;

#[wasm_bindgen]
#[derive(Debug, PartialEq)]
pub enum Player {
    None,
    O,
    X,
}
