export default {
  content: ['./index.html', './src/**/*.{vue,js,ts}'],
  theme: {
    container: {
      center: true,
    },
    extend: {
      colors: {
        body: '#f2f1ed',
        text: '#666c6b',
        header: '#00AF6B',
        primary: '#fff07b',
      },
      fontFamily: {
        Roboto: ['Roboto'],
      },
    },
  },
  plugins: [],
};
