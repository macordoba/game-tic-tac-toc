import * as wasm from 'tic-tac-toc';

const player_converter = (player: wasm.Player) => {
  switch (player) {
    case wasm.Player.X:
      return 'X';
    case wasm.Player.O:
      return 'O';
  }
};

export { player_converter };
